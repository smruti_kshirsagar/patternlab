const gulp = require('gulp');
const sass = require('gulp-sass');
const del = require('del');

gulp.task('styles', () => {
    return gulp.src('public/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('public/'));
});
gulp.task('clean', () => {
    return del([
        'css/style.css',
    ]);
});

gulp.task('default', gulp.series(['clean', 'styles']));